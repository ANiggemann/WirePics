# WirePics
The purpose of this Java utility as a web service is to display images fullscreen on a remote computer screen.<br>
Typical usages are digital signage applications, remote controlled slideshows, info walls and other display and advertising functions.  
Each monitor in this applications has its own computer (i.e. Raspberry Pi etc.) and should run one instance of WirePics.<br>

The remote computer should be on the same network (LAN/WiFi etc.) and can be addressed via IP or hostname.<br>
In the examples below localhost is used to address the computer.  Replace localhost with the IP address or hostname of your choice.<br>
The service can be run on 1 to n computer and each service on one of the units can be controlled from other machines on the same network.<br>

By default WirePics services are listening on port 8080. This can be changed via command line parameter (see below).

Till today only JPG images can be processed and displayed by WirePics.

# Features
* Platform independent by using a Java JAR
* Configurable server port
* Resize each image to full screen
* Shows an unlimited number of images
* Internal image storage for fast recall of already uploaded images
* Option of limiting the internal image storage so that older images are dropped
* Informations including the content of the image storage available via browser requests
* Extensive help screen in browser
* Remote control via http request
  * Service run on a computer and can be controlled via network from an other machine
  * Picture upload and show
  * Play image slideshow via playlists
  * Play playlist once or endlessly
  * Stop, pause and continue any playlist via http requests
  * Move forward and backwards thru the images in the internal image storage
  * Jump to the first image in the internal image storage
  * Stopping the service per http request
  * Change remotely shown image by clicking on thumbnail in the internal image storage list

# Building WirePics
* Clone the repository https://gitlab.com/ANiggemann/WirePics.git
* Start gradle task bootJar (gradlew bootJar or ./gradlew bootJar)
* Find the WirePics.jar in the project directory

# Start of WirePics service
Via command line:<br>
java -jar WirePics.jar

# Checking if WirePics is running
Use localhost:8080/info as the URL in a browser.<br>
Response should be similar to this:<br>
WirePics V1.0<br>
Images displayed: 0<br>
(or any number of images)

# Uploading image to WirePics and show the image
Use POST requests via Postman or cURL.<br>
Example:<br>
curl -F "image=@images/2.jpg" -F "name=this image name" localhost:8080/image<br>
This uploads the image 2.jpg from the images folder to the service and stores it under the name
this image name in the internal image storage for later recall. Then the image will be shown.

# Uploading image to WirePics internal image storage
Use POST requests via Postman or cURL.<br>
Example:<br>
curl -F "image=@images/2.jpg" -F "name=this image name" localhost:8080/upload<br>
This uploads the image 2.jpg from the images folder to the service and stores it under the name
this image name in the internal image storage for later recall.

# Stopping the service
Send an EXIT signal to WirePics.<br>
Example:<br>
curl -F "command=EXIT" localhost:8080/control<br>
or<br>
localhost:8080/exit via browser.

# Change server port
Via command line parameter at startup<br>
Example:<br>
java -jar WirePics.jar --server.port=7663

# Getting help information
Use localhost:8080/help or localhost:8080/h as the URL in a browser.

# Show the internal image storage
Use localhost:8080/list or localhost:8080/l as the URL in a browser.

# Commands
Stopping the service:<br>
curl -F "command=EXIT" localhost:8080/cmd<br>
Clearing the internal image storage:<br>
curl -F "command=CLEAR" localhost:8080/cmd<br>
Switch to upload only (no images are shown):<br>
curl -F "command=UPLOADONLY" localhost:8080/cmd<br>
Switch to upload and show (images are shown):<br>
curl -F "command=UPLOADANDSHOW" localhost:8080/cmd<br>
Limit the internal image storage to 100 images:<br>
curl -F "command=LIMIT" -F "parameter=100" localhost:8080/cmd<br>
Show an image named "this_car" thats in the internal image storage:<br>
curl -F "command=SHOW" -F "parameter=this_car" localhost:8080/cmd<br>
Show first image in the image storage:<br>
curl -F "command=FIRST" localhost:8080/cmd<br>
Forward one image in the image storage and show it:<br>
curl -F "command=FORWARD" localhost:8080/cmd<br>
Backward one image in the image storage and show it:<br>
curl -F "command=BACKWARD" localhost:8080/cmd<br>
Play a slideshow of all the images in the internal image storage with pauses of 5 seconds:<br>
curl -F "command=PLAY" -F "parameter=5 ALL" localhost:8080/cmd<br>
Play a slideshow of all the referenced images in the internal image storage with pauses of 15 seconds:<br>
curl -F "command=PLAY" -F "parameter=15 'this_car' 'this_house' 'this_boat'" localhost:8080/cmd<br>
Play a slideshow endlessly of all the referenced images in the internal image storage with pauses of 15 seconds:<br>
curl -F "command=PLAY" -F "parameter=-15 'this_car' 'this_house' 'this_boat'" localhost:8080/cmd<br>
(Endless because of the negative pause time)<br>
Stopping the playlist:<br>
curl -F "command=STOP" localhost:8080/cmd<br>
Pausing the playlist:<br>
curl -F "command=PAUSE" localhost:8080/cmd<br>
Continuing the playlist:<br>
curl -F "command=CONTINUE" localhost:8080/cmd<br>

# URL endpoints and request parameters
POST /upload<br>
* multipart image<br>
* String name<br>

POST /u<br>
* Same as /upload<br>

POST /up<br>
* Same as /upload<br>

POST /image<br>
* multipart image<br>
* String name<br>

POST /img<br>
* Same as /image<br>

POST /i<br>
* Same as /image<br>

POST /control<br>
* String command<br>
* String params<br>
* String comment<br>
  
POST /cmd<br>
* Same as /control<br>
  
POST /ctrl<br>
* Same as /control<br>

GET /help<br>
GET /h<br>
GET /status<br>
GET /info<br>
GET /list<br>
GET /exit<br>
GET /leave<br>
GET /quit<br>
GET /q<br>
GET /x<br>
GET /show?parameter=imagename<br>
GET /s?parameter=imagename<br>
GET /stop<br>
GET /t<br>
GET /pause<br>
GET /p<br>
GET /continue<br>
GET /c<br>
GET /clear<br>
GET /empty<br>
GET /e<br>
GET /delete?parameter=imagename<br>
GET /remove?parameter=imagename<br>
GET /d?parameter=imagename<br>
GET /r?parameter=imagename<br>
GET /limit?parameter=limitnumberofentries<br>
GET /m?parameter=limitnumberofentries<br>
GET /play?parameter=pausetime listofimagenames<br>
GET /y?parameter=pausetime listofimagenames<br>
GET /first<br>
GET /f<br>
GET /forward<br>
GET /w<br>
GET /backward<br>
GET /b<br>
