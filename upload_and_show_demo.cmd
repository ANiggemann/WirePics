SET PORT=8080

curl -F "image=@images/1.jpg" -F "name=File 1" localhost:%PORT%/image
PING localhost -n 7 >NUL
curl -F "image=@images/2.jpg" -F "name=File 2" localhost:%PORT%/image
PING localhost -n 5 >NU
curl -F "image=@images/3.jpg" -F "name=File 3" localhost:%PORT%/image
PING localhost -n 5 >NUL
curl -F "image=@images/4.jpg" -F "name=File 4" localhost:%PORT%/image
PING localhost -n 5 >NUL
curl -F "image=@images/5.jpg" -F "name=File 5" localhost:%PORT%/image
PING localhost -n 5 >NUL
curl -F "image=@images/6.jpg" -F "name=File 6" localhost:%PORT%/image
PING localhost -n 5 >NUL

curl -F "command=EXIT" -F "comment=Stop service" localhost:%PORT%/cmd

exit
