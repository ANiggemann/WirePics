package biz.niggemann.wirepics.controllers;

import biz.niggemann.wirepics.helpers.DelayedExit;
import biz.niggemann.wirepics.helpers.InfoScreens;
import biz.niggemann.wirepics.helpers.Thumbnails;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;


@SuppressWarnings("unused")
@RestController
public class WirePicsController implements ErrorController {

    private JFrame frame = null;
    private long imageCounter = 0;
    private int storageLimit = Integer.MAX_VALUE;
    private String actuFilename = "";
    private String slideShowList = "";
    private InfoScreens iScreens = new InfoScreens();
    private Thumbnails thumbs = new Thumbnails();
    private LinkedHashMap<String, BufferedImage> imageStorage = new LinkedHashMap<>();

    private AtomicInteger pauseTime = new AtomicInteger(-1);
    private AtomicInteger playlistRunning = new AtomicInteger(0); // 0=not running, 1=paused, 2=running
    private AtomicBoolean playlistNonStop = new AtomicBoolean(false); // true = repeat playlist endlessly

    @Value("${server.port:8080}")
    private String serverPort;

    // POST commands
    @PostMapping(value = {"/upload", "/up", "/u"})
    @ResponseBody
    private String imageUpload(@RequestParam(value = "image") MultipartFile img,
                               @RequestParam(value = "name", required = false, defaultValue = "") String name) {
        return processUpload(img, name, false); // upload, store it into internal image storage
    }

    @PostMapping(value = {"/image", "/img", "/i"})
    @ResponseBody
    private String imageUploadAndShow(@RequestParam(value = "image") MultipartFile img,
                                      @RequestParam(value = "name", required = false, defaultValue = "") String name) {
        return processUpload(img, name, true); // upload, store it into internal image storage and show it
    }

    @PostMapping(value = {"/control", "/cmd", "/ctrl"})
    @ResponseBody
    private String signalCmd(@RequestParam(value = "command") String command,
                             @RequestParam(value = "parameter", required = false, defaultValue = "") String params,
                             @RequestParam(value = "comment", required = false, defaultValue = "") String comment) {
        String retVal = execRequest(command, params, false);
        if (!comment.equals("")) // Comment was there
            System.out.println("Command: " + retVal + ", " + comment);
        return retVal;
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }

    @RequestMapping(value = "/error")
    private String errorPage() {
        return execRequest("i", "", true);
    }

    // GET commands
    @GetMapping(value = "/{exec}")
    @ResponseBody
    private String execGETRequest(@PathVariable String exec,
                                  @RequestParam(value = "parameter", required = false, defaultValue = "") String params) {
        return execRequest(exec, params, true);
    }

    @Scheduled(fixedRate = 2000)
    private void runSlideshow() {
        if (thumbs.size() != imageStorage.size())
            thumbs.generateMissingThumbnails(imageStorage, 1);
        if (pauseTime.get() > -1) // playlist there
            playSlideshow(pauseTime.get(), slideShowList, playlistNonStop.get());
    }

    private String execShowImage(String params, boolean wasGETRequest) {
        String retVal = "Image name missing";
        if (!params.equals("")) // Image name is there
            if (imageStorage.containsKey(params)) {
                String imageString = "";
                if (wasGETRequest)
                    imageString = thumbs.generateThumbnailToHTML(params, imageStorage.get(params), 200, false);
                if (showImage(params))
                    retVal = "SHOW<br>" + params + "<br>" + imageString; // Show small image via response
            } else
                retVal = "Image " + params + " missing";
        return retVal;
    }

    private String execPlayList(String params, String retVal) {
        if (!params.equals("")) {// pause time and playlist is there
            String[] parameters = params.split(" ", 2); // pause time and list
            if (parameters.length >= 2) {
                int pTime;
                int paramStartIdx = 1;
                try {
                    pTime = Integer.parseInt(parameters[0]);
                } catch (NumberFormatException nfe) {
                    pTime = 5;
                    paramStartIdx = 0; // First parameter must be an image name
                }
                playlistNonStop.set(pTime < 0); // Play endless if pause time negative
                pauseTime.set(Math.abs(pTime));
                slideShowList = parameters[paramStartIdx];
            }
        } else
            retVal = "Pause time and/or playlist missing";
        return retVal;
    }

    private String execFirstForwardbackward(int direction, boolean wasGETRequest) {
        String retVal = "";
        if (imageStorage.size() > 0) {
            ArrayList<String> keys = new ArrayList<>(imageStorage.keySet());
            int keyPos = (direction != 0) ? keys.indexOf(actuFilename) : 0;
            if ((direction == 0) ||
                    ((direction == +1) && (keyPos < (keys.size() - 1))) ||
                    ((direction == -1) && (keyPos > 0))) {
                if (wasGETRequest)
                    retVal = thumbs.generateThumbnailToHTML(keys.get(keyPos + direction), imageStorage.get(keys.get(keyPos + direction)), 200, false);
                showImage(keys.get(keyPos + direction));
            }
        }
        if (retVal.equals(""))
            retVal = iScreens.getInfoScreens(2, serverPort, imageCounter, imageStorage, thumbs);
        else
            retVal += iScreens.getInfoScreens(3, serverPort, imageCounter, imageStorage, thumbs);
        return retVal;
    }

    private String execRequest(String exec, String params, boolean wasGETRequest) {
        String retVal = exec.toUpperCase() + ((!params.equals("")) ? params : "");
        switch (exec.toLowerCase()) {
            case "show":
            case "s":
                retVal = execShowImage(params, wasGETRequest);
                break;
            case "stop":
            case "t":
                playlistRunning.set(0);
                break;
            case "pause":
            case "p":
                playlistRunning.set(1);
                break;
            case "continue":
            case "c":
                playlistRunning.set(2);
                break;
            case "clear":
            case "empty":
            case "e":
                imageStorage.clear();
                retVal = "Internal image storage cleared";
                break;
            case "delete":
            case "remove":
            case "d":
            case "r":
                if (!params.equals("")) // Image name is there, delete it
                {
                    thumbs.remove(params);
                    imageStorage.remove(params);
                }
                break;
            case "limit":
            case "m":
                try {
                    storageLimit = Integer.parseInt(params);
                } catch (NumberFormatException nfe) {
                    storageLimit = Integer.MAX_VALUE;
                    retVal = "Maximum numbers of entries missing";
                }
                break;
            case "play":
            case "y":
                retVal = execPlayList(params, retVal);
                break;
            case "first":
            case "f":
                retVal = execFirstForwardbackward(0, wasGETRequest);
                break;
            case "forward":
            case "w":
                retVal = execFirstForwardbackward(+1, wasGETRequest);
                break;
            case "backward":
            case "b":
                retVal = execFirstForwardbackward(-1, wasGETRequest);
                break;
            case "list":
            case "l":
                retVal = iScreens.getInfoScreens(5, serverPort, imageCounter, imageStorage, thumbs);
                break;
            case "exit":
            case "leave":
            case "quit":
            case "x":
            case "q":
                retVal = iScreens.getWirepicsName() + " shutting down...";
                new DelayedExit(5); // 5 seconds delay till exit to enable response send
                break;
            case "help":
            case "h":
                retVal = iScreens.getInfoScreens(1, serverPort, imageCounter, imageStorage, thumbs);
                break;
            case "info":
            case "status":
            case "i":
            default:
                retVal = iScreens.getInfoScreens(2, serverPort, imageCounter, imageStorage, thumbs) +
                        iScreens.getInfoScreens(3, serverPort, imageCounter, imageStorage, thumbs) +
                        iScreens.getInfoScreens(4, serverPort, imageCounter, imageStorage, thumbs);
                break;
        }
        return retVal;
    }

    private String processUpload(MultipartFile imgFile, String uploadedName, boolean showUploadedImage) {
        String retVal = "not uploaded";
        if (imgFile != null) {
            try {
                byte[] imageData = imgFile.getBytes();
                BufferedImage actuImage = ImageIO.read(new ByteArrayInputStream(imageData));
                if (actuImage != null) {
                    retVal = "uploaded";
                    storeImage(actuImage, uploadedName);
                    if (showUploadedImage)
                        showImage(uploadedName);
                }
            } catch (IOException e) {
                iScreens.logStackTrace(e);
            }
        }
        if (!uploadedName.equals("")) // Name was there
            System.out.println("Image: " + uploadedName + " " + retVal);
        return retVal;
    }

    private void storeImage(BufferedImage img, String name) {
        String actuDateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm_ss_SSS"));
        String actuName = (name.equals("")) ? "_internal_name_" + actuDateTime : name;
        imageStorage.remove(actuName);  // if same name exists remove old one to prevent duplicates
        thumbs.remove(actuName);
        if (imageStorage.size() >= storageLimit) // Remove first and oldest element if limit reached
        {
            ArrayList<String> keys = new ArrayList<>(imageStorage.keySet());
            String nameToRemove = keys.get(0); // oldest image
            imageStorage.remove(nameToRemove);
        }
        imageStorage.put(actuName, img);
    }

    private boolean showImage(String imgName) {
        boolean retVal = false; // if image could not be shown
        BufferedImage img = imageStorage.get(imgName);
        if (img != null) {
            if (frame == null) { // do it only at the first image
                frame = new JFrame("WirePics");
                frame.setUndecorated(true);
                GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
                GraphicsDevice gs = ge.getDefaultScreenDevice();
                gs.setFullScreenWindow(frame);
            } else
                frame.getContentPane().removeAll(); // Remove last image if there
            frame.add(new Component() {
                @Override
                public void paint(Graphics g) {
                    super.paint(g);
                    g.drawImage(img, 0, 0, getWidth(), getHeight(), this); // Add image to frame
                }
            });
            frame.validate();
            imageCounter++;
            actuFilename = imgName;
            retVal = true;
        }
        return retVal;
    }

    private String[] preparePlaylist(String playList) {
        String playL;
        StringBuilder sb = new StringBuilder();
        if (playList.toUpperCase().trim().equals("ALL")) {
            for (Map.Entry<String, BufferedImage> actuNamedImage : imageStorage.entrySet())
                sb.append(",").append(actuNamedImage.getKey());
            playL = sb.toString().substring(1).trim();
        } else
            playL = playList;
        return playL.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)"); // CSV split
    }

    private int playSingleSlideWithTiming(String imageName, int imgIndex, int pTime) {
        int waitTimer = 250; // 250ms wait in pause mode
        if (playlistRunning.get() == 2) // Still running
        {
            waitTimer = (showImage(imageName)) ? pTime * 1000 : 0; // No wait if image not there
            imgIndex++; // Next image only if running
        }
        if (waitTimer > 0) {
            try { // Wait after one image or in pause mode (playlistRunning = 1)
                Thread.sleep(waitTimer);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
        return imgIndex;
    }

    private void playSlideshow(int pTime, String playList, boolean playEndlessly) {
        pauseTime.set(-1);
        slideShowList = "";
        String[] imageNameList = preparePlaylist(playList);
        do { // For endless play
            playlistRunning.set(2); // Running flag
            int imgIndex = 0;
            while (imgIndex < imageNameList.length) {
                if (playlistRunning.get() == 0) // stop signaled
                {
                    playEndlessly = false;
                    playlistNonStop.set(false);
                    break;
                } else
                    imgIndex = playSingleSlideWithTiming(imageNameList[imgIndex].trim(), imgIndex, pTime);
            }
        } while (playEndlessly);
        playlistRunning.set(0); // End of playlist -> stop
    }
}
