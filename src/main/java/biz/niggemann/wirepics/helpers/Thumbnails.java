package biz.niggemann.wirepics.helpers;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Thumbnails {

    private HashMap<String, String> thumbnailCache = new HashMap<>();

    public int size() {
        return thumbnailCache.size();
    }

    public void remove(String name) {
        thumbnailCache.remove(name);
    }

    public void generateMissingThumbnails(LinkedHashMap<String, BufferedImage> imgStorage, int generateMode) { // 0 = generate all, 1 = generate one missing, 2 = generate all missing
        for (Map.Entry<String, BufferedImage> actuNamedImage : imgStorage.entrySet())
            if (!thumbnailCache.containsKey(actuNamedImage.getKey()) || (generateMode == 0)) { // generate thumbnail
                generateThumbnailToHTML(actuNamedImage.getKey(), actuNamedImage.getValue(), 100, true);
                if (generateMode == 1)
                    break; // generated only one missing thumbnail at a time
            }
    }

    public String generateThumbnailToHTML(String imgName, BufferedImage img, int width, boolean useCache) {
        String data;
        if (thumbnailCache.containsKey(imgName) && useCache) { // get thumbnail from cache
            data = thumbnailCache.get(imgName);
        } else { // generate new thumbnail
            Image imgSmall = img.getScaledInstance(width, -1, Image.SCALE_SMOOTH);
            BufferedImage dimg = new BufferedImage(imgSmall.getWidth(null), imgSmall.getHeight(null), Image.SCALE_REPLICATE);
            dimg.getGraphics().drawImage(imgSmall, 0, 0, null);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                ImageIO.write(dimg, "jpg", baos);
            } catch (IOException e) {
                e.printStackTrace();
            }
            data = DatatypeConverter.printBase64Binary(baos.toByteArray());
            if (useCache)
                thumbnailCache.put(imgName, data);
        }
        return "<img src='data:image/jpg;base64," + data + "'>";
    }
}
