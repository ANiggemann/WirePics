package biz.niggemann.wirepics.helpers;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class DelayedExit {

    public DelayedExit(int waitSeconds) {
        Timer timer = new Timer();
        TimerTask exitApp = new TimerTask() {
            public void run() {
                System.exit(0);
            }
        };
        timer.schedule(exitApp, new Date(System.currentTimeMillis() + waitSeconds * 1000));
    }
}
