package biz.niggemann.wirepics.helpers;

import java.awt.image.BufferedImage;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;

public class InfoScreens {

    public String getWirepicsName() {
        final String wirepicsVersion = "1.0";
        return "WirePics V" + wirepicsVersion;
    }

    private String getBaseURL(String srvPort) {
        InetAddress ip;
        String baseUrl = "http://localhost:8080";
        try {
            ip = InetAddress.getLocalHost();
            baseUrl = "http://" + ip.getHostAddress() + ":" + srvPort;
        } catch (UnknownHostException e) {
            logStackTrace(e);
        }
        return baseUrl;
    }

    public String getInfoScreens(int screenNumber, String srvPort, Long imgCounter,
                                 LinkedHashMap<String, BufferedImage> imgStorage, Thumbnails thumbnails) {
        String retVal = "";
        final String LINKSTARTHTML = "<a href=\"";
        switch (screenNumber) {
            case 1:
                String cmdFormat = "&nbsp;&nbsp;curl -F \"command=%s\" &ltip or hostname&gt:8080/cmd</br>";
                retVal = getWirepicsName() + "</br></br>" +
                        "Start of WirePics service:</br>" +
                        "&nbsp;&nbsp;java -jar WirePics.jar</br></br>" +
                        "Change server port via command line parameter:<br>" +
                        "&nbsp;&nbsp;java -jar WirePics.jar --server.port=7663</br></br>" +
                        "Checking if WirePics is running:</br>" +
                        "&nbsp;&nbsp;&ltip or hostname&gt:8080/info as the URL in a browser.</br>" +
                        "Uploading images to WirePics using POST requests via Postman or cURL:</br>" +
                        "&nbsp;&nbsp;curl -F \"image=@images/2.jpg\" -F \"name=this_car\" &ltip or hostname&gt:8080/upload</br>" +
                        "<br>" +
                        "Stopping the service:</br>" +
                        String.format(cmdFormat, "EXIT") +
                        "Clearing the internal image storage:</br>" +
                        String.format(cmdFormat, "CLEAR") +
                        "Switch to upload only (no images are shown):</br>" +
                        String.format(cmdFormat, "UPLOADONLY") +
                        "Switch to upload and show (images are shown):</br>" +
                        String.format(cmdFormat, "UPLOADANDSHOW") +
                        "Limit the internal image storage to 100 images:</br>" +
                        "&nbsp;&nbsp;curl -F \"command=LIMIT\" -F \"parameter=100\" &ltip or hostname&gt:8080/cmd</br>" +
                        "<br>" +
                        "Show an image named \"this_car\" thats in the internal image storage:</br>" +
                        "&nbsp;&nbsp;curl -F \"command=SHOW\" -F \"parameter=this_car\" &ltip or hostname&gt:8080/cmd</br>" +
                        "Show first image in the image storage:</br>" +
                        String.format(cmdFormat, "FIRST") +
                        "Forward one image in the image storage and show it:</br>" +
                        String.format(cmdFormat, "FORWARD") +
                        "Backward one image in the image storage and show it:</br>" +
                        String.format(cmdFormat, "BACKWARD") +
                        "<br>" +
                        "Play a slideshow of all the images in the internal image storage with pauses of 5 seconds:</br>" +
                        "&nbsp;&nbsp;curl -F \"command=PLAY\" -F \"parameter=5 ALL\" &ltip or hostname&gt:8080/cmd</br>" +
                        "Play a slideshow of all the referenced images in the internal image storage with pauses of 15 seconds:</br>" +
                        "&nbsp;&nbsp;curl -F \"command=PLAY\" -F \"parameter=15 'this_car' 'this_house' 'this_boat'\" &ltip or hostname&gt:8080/cmd</br>" +
                        "Stopping the playlist:</br>" +
                        String.format(cmdFormat, "STOP") +
                        "Pausing the playlist:</br>" +
                        String.format(cmdFormat, "PAUSE") +
                        "Continuing the playlist:</br>" +
                        String.format(cmdFormat, "CONTINUE") +
                        "";
                break;
            case 2:
                String extraInfoHelp = "<br><br>"+LINKSTARTHTML + getBaseURL(srvPort) + "/help\">More help</a><br><br>";
                String extraInfoStorage = LINKSTARTHTML + getBaseURL(srvPort) + "/list\">Image storage</a>";
                retVal = getWirepicsName() + "</br>Images displayed: " + Long.toString(imgCounter) +
                        extraInfoHelp + extraInfoStorage;
                break;
            case 3:
                retVal = "<br>" + LINKSTARTHTML + getBaseURL(srvPort) + "/first\">First image</a> ";
                retVal += LINKSTARTHTML + getBaseURL(srvPort) + "/forward\">Next image</a> ";
                retVal += LINKSTARTHTML + getBaseURL(srvPort) + "/backward\">Previous image</a></br>";
                break;
            case 4:
                retVal += "<br>Playlist:</br>";
                retVal += LINKSTARTHTML+ getBaseURL(srvPort) + "/stop\">Stop</a> ";
                retVal += LINKSTARTHTML + getBaseURL(srvPort) + "/pause\">Pause</a> ";
                retVal += LINKSTARTHTML + getBaseURL(srvPort) + "/continue\">Continue</a></br>";
                break;
            case 5: // Show internal image storage
                retVal = getWirepicsName() + "</br></br>Content of internal image storage:</br>";
                if (imgStorage.size() > 0) {
                    StringBuilder sb = new StringBuilder();
                    int idx = 1;
                    sb.append("<table><tr>");
                    for (Map.Entry<String, BufferedImage> actuNamedImage : imgStorage.entrySet()) {
                        String name = actuNamedImage.getKey();
                        String imgString = thumbnails.generateThumbnailToHTML(name, actuNamedImage.getValue(), 100, true);
                        try {
                            name = URLEncoder.encode(name, StandardCharsets.UTF_8.toString());
                        } catch (UnsupportedEncodingException e) {
                            logStackTrace(e);
                        }
                        String imageStringWithLink = LINKSTARTHTML + getBaseURL(srvPort) +
                                "/show?parameter=" + name + "\">" + imgString + "</a>";
                        sb.append("<td><figure style=\"margin-left:10px;margin-right:10px;\"><figcaption>")
                                .append(actuNamedImage.getKey()).append("</figcaption>")
                                .append(imageStringWithLink).append("</figure></td>");
                        if ((idx % 4) == 0)
                            sb.append("</tr><tr>");
                        idx++;
                    }
                    sb.append("</tr></table>");
                    retVal += sb.toString();
                }
                break;
            default:
                break;
        }
        return retVal;
    }

    public void logStackTrace(Exception e)
    {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        String exceptionAsString = sw.toString();
        System.out.println(exceptionAsString);
    }
}
